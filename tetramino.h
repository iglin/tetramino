//
// Created by root on 12/20/17.
//

#ifndef LAB2_TETRAMINO_H
#define LAB2_TETRAMINO_H

#include <vector>
#include <chrono>
#include "lab_utils.h"

using namespace std;

struct Figure {
    int turns = 0;
    Matrix<bool> matrix = Matrix<bool>(0, 0); // bool matrix: true - cell is filled, false - cell is clear

    Figure() = default;

    explicit Figure(int turns) {
        this->turns = turns;
    }

    bool at(int i, int j) {
        return matrix.elements[i][j];
    }

    bool rotate() {
        if (turns == 0) return false;
        turns--;
        matrix.rotate();
        return true;
    }
};

struct Field {
    int size = 8;
    int **cells;

    Field() {
        cells = new int*[size];
        for (int i = 0; i < size; i++) {
            cells[i] = new int[size];
            for (int j = 0; j < size; j++) {
                cells[i][j] = -1;
            }
        }
    }

    explicit Field(int size) {
        this->size = size;
        cells = new int*[size];
        for (int i = 0; i < size; i++) {
            cells[i] = new int[size];
            for (int j = 0; j < size; j++) {
                cells[i][j] = -1;
            }
        }
    }

    bool insert(Figure *figure, int x, int y, int iteration) {
        for (int i = 0; i < figure->matrix.n; i++) {
            for (int j = 0; j < figure->matrix.m; j++) {
                if (figure->at(i, j)) {
                    if (cells[x + i][y + j] != -1) return false;
                }
            }
        }

        for (int i = 0; i < figure->matrix.n; i++) {
            for (int j = 0; j < figure->matrix.m; j++) {
                if (figure->at(i, j)) {
                    cells[x + i][y + j] = iteration;
                }
            }
        }
        return true;
    }

    void remove(Figure *figure, int x, int y) {
        for (int i = 0; i < figure->matrix.n; i++) {
            for (int j = 0; j < figure->matrix.m; j++) {
                if (figure->at(i, j)) {
                    cells[x + i][y + j] = -1;
                }
            }
        }
    }
};

class Tetramino {
private:
    vector<Figure> *figures;
    Field *field;

    chrono::time_point<chrono::high_resolution_clock, chrono::nanoseconds> begin;

    vector<Figure> *initFigures() {
        auto result = new vector<Figure>();

        Figure *figure;
        // x x x x
        figure = new Figure(1);
        figure->matrix = Matrix<bool>(1, 4);
        for (int i = 0; i < 4; i++) figure->matrix.elements[0][i] = true;
        result->push_back(*figure);

        // x x x
        // x
        figure = new Figure(3);
        figure->matrix = Matrix<bool>(2, 3);
        figure->matrix.elements[0][0] = true; figure->matrix.elements[0][1] = true; figure->matrix.elements[0][2] = true;
        figure->matrix.elements[1][0] = true; figure->matrix.elements[1][1] = false; figure->matrix.elements[1][2] = false;
        result->push_back(*figure);

        // x
        // x x x
        figure = new Figure(3);
        figure->matrix = Matrix<bool>(2, 3);
        figure->matrix.elements[0][0] = true; figure->matrix.elements[0][1] = false; figure->matrix.elements[0][2] = false;
        figure->matrix.elements[1][0] = true; figure->matrix.elements[1][1] = true; figure->matrix.elements[1][2] = true;
        result->push_back(*figure);

        // x x x
        //   x
        figure = new Figure(3);
        figure->matrix = Matrix<bool>(2, 3);
        figure->matrix.elements[0][0] = true; figure->matrix.elements[0][1] = true; figure->matrix.elements[0][2] = true;
        figure->matrix.elements[1][0] = false; figure->matrix.elements[1][1] = true; figure->matrix.elements[1][2] = false;
        result->push_back(*figure);

        // x x
        //   x x
        figure = new Figure(1);
        figure->matrix = Matrix<bool>(2, 3);
        figure->matrix.elements[0][0] = true; figure->matrix.elements[0][1] = true; figure->matrix.elements[0][2] = false;
        figure->matrix.elements[1][0] = false; figure->matrix.elements[1][1] = true; figure->matrix.elements[1][2] = true;
        result->push_back(*figure);

        //   x x
        // x x
        figure = new Figure(1);
        figure->matrix = Matrix<bool>(2, 3);
        figure->matrix.elements[0][0] = false; figure->matrix.elements[0][1] = true; figure->matrix.elements[0][2] = true;
        figure->matrix.elements[1][0] = true; figure->matrix.elements[1][1] = true; figure->matrix.elements[1][2] = false;
        result->push_back(*figure);

        // x x
        // x x
        figure = new Figure(1);
        figure->matrix = Matrix<bool>(2, 2);
        figure->matrix.elements[0][0] = true; figure->matrix.elements[0][1] = true;
        figure->matrix.elements[1][0] = true; figure->matrix.elements[1][1] = true;
        result->push_back(*figure);

        return result;
    }

    bool checkFinished() {
        for (int i = 0; i < field->size; i++) {
            for (int j = 0; j < field->size; j++) {
                if (field->cells[i][j] == -1) return false;
            }
        }
        return true;
    }

    bool solveRecursive(int *iteration, int depth) {
        if (checkFinished()) return true;

        *iteration = *iteration + 1;
        //int currentIteration = *iteration;

       // chrono::time_point<chrono::high_resolution_clock, chrono::nanoseconds> end = chrono::high_resolution_clock::now();
        cout << *iteration << " iteration "
             << chrono::duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now()
                                                           - begin).count() / 1000000000.0 << " sec" << endl;

        cout << "DEPTH " << depth << endl;
        //srand(time(NULL));
        int r;
        vector<Figure> figuresCopy = *figures;
        while (!figuresCopy.empty()) {
            r = rand() % figuresCopy.size();
            cout << " r = " << r << endl;
            Figure figure = figuresCopy.at(r);
            figuresCopy.erase(figuresCopy.begin() + r);
            do {
                for (int i = 0; i < field->size - figure.matrix.n + 1; i++) {
                    for (int j = 0; j < field->size - figure.matrix.m + 1; j++) {
                        if (field->insert(&figure, i, j, depth)) {
                            cout << "Succeed on iteration " << *iteration << endl;
                            printMatrix(field->cells, field->size, field->size);
                            if (solveRecursive(iteration, depth + 1)) return true;
                            else {
                                field->remove(&figure, i, j);
                                //cout << "Rolled back to iteration " << currentIteration << endl;
                                cout << "Rolled back" << endl;
                                printMatrix(field->cells, field->size, field->size);
                            }
                        }
                    }
                }
            } while (figure.rotate());

            //delete figure;
        }

        //delete figuresCopy;

        return false;
    }
public:
    Tetramino() {
        field = new Field();
        figures = initFigures();
    }

    explicit Tetramino(int size) {
        field = new Field(size);
        figures = initFigures();
    }

    int solve() {
        begin = chrono::high_resolution_clock::now();
        int *iteration = new int;
        *iteration = 0;
        srand(time(NULL));
        solveRecursive(iteration, 1);
        cout << "Result:" << endl;
        printMatrix(field->cells, field->size, field->size);
        return *iteration;
    }

//    int solve() {
//        int safe = 0;
//        int iteration = 1;
//        srand(time(NULL));
//        int r;
//        while (!checkFinished()) {
//            safe++;
//            if (safe > 1000)
//                return -1;
//            bool succeed = false;
//            vector<Figure> figuresCopy = *figures;
//            while (!succeed && !figuresCopy.empty()) {
//                r = rand() % figuresCopy.size();
//                Figure figure = figuresCopy.at(r);
//                //cout << "before : " << figuresCopy.size() << endl;
//                figuresCopy.erase(figuresCopy.begin() + r);
//
//                //cout << "after : " << figuresCopy.size() << endl;
//                do {
//                    for (int i = 0; i < field->size - figure.matrix.n + 1; i++) {
//                        for (int j = 0; j < field->size - figure.matrix.m + 1; j++) {
//                            succeed = field->insert(figure, i, j, iteration);
//                            if (succeed) break;
//                        }
//                        if (succeed) break;
//                    }
//                    //cout << "Could rotate =" << couldRotate << endl;
//                } while (!succeed && figure.rotate());
//            }
//            if (succeed) {
//                cout << "Succeed on iteration " << iteration <<endl;
//                printMatrix(field->cells, field->size, field->size);
//                iteration++;
//            } else {
//                // TODO: remove checking
//                if (iteration == 1) return -1;
//                iteration--;
//                for (int i = 0; i < field->size; i++) {
//                    for (int j = 0; j < field->size; j++) {
//                        if (field->cells[i][j] == iteration) field->cells[i][j] = -1;
//                    }
//                }
//                cout << "Rolled back to iteration " << iteration <<endl;
//                printMatrix(field->cells, field->size, field->size);
//            }
//        }
//        return iteration;
//    }
};

#endif //LAB2_TETRAMINO_H
