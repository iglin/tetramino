//
// Created by root on 12/20/17.
//

#ifndef LAB2_LAB_UTILS_H
#define LAB2_LAB_UTILS_H

#include <iostream>
#include <boost/dynamic_bitset.hpp>

using namespace std;
using namespace boost;

void populateBitSet (std::string &buffer, boost::dynamic_bitset<unsigned char> & bitMap) {
    bitMap = boost::dynamic_bitset<unsigned char> (buffer);
}

dynamic_bitset<unsigned char> subset(const dynamic_bitset<unsigned char> &bits, int startIndex, int length) {
    dynamic_bitset<unsigned char> result = dynamic_bitset<unsigned char>(length);
    int idx = startIndex;
    for (int i = 0; i < length; i++) {
        result[i] = (bits[idx]);
        idx++;
    }
    return result;
}

template<typename T>
void printMatrix(T **matrix, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cout << " ";
            if (matrix[i][j] > -1 && matrix[i][j] < 10) cout << " ";
            cout << matrix[i][j];
        }
        cout << endl;
    }
}

template<typename T> struct Matrix {
    T **elements;
    int n, m;

    Matrix(int n, int m) {
        this->n = n;
        this->m = m;
        elements = new T*[n];
        for (int i = 0; i < n; i++) {
            elements[i] = new T[m];
        }
    }

    Matrix(T **input, int n, int m) : Matrix(n, m) {
        elements = input;
    }

    void rotate() {
        int newN = m;
        int newM = n;
        T **newMatrix = new T*[newN];
        for (int i = 0; i < newN; i++) {
            newMatrix[i] = new T[newM];
            for (int j = 0; j < newM; j++) {
                newMatrix[i][j] = elements[n - j - 1][i];
            }
        }
        n = newN;
        m = newM;
        elements = newMatrix;
    }

    void print() {
        printMatrix<T>(elements, n, m);
    }
};


#endif //LAB2_LAB_UTILS_H
