#include <iostream>
#include "tetramino.h"
#include "tetramino_bits.h"

int main() {

    Tetramino *tetramino = new Tetramino(6);
    //cout << tetramino->solve() << endl;

    TetraminoBits *tetraminoBits = new TetraminoBits(6);
    cout << tetraminoBits->solve() << endl;


//    // x x x
//    // x
//    BitFigure *bitFigure = new BitFigure(2, 3);
//    bitFigure->turns = 3;
//    bitFigure->rows.at(0) = dynamic_bitset<unsigned char>(string("111"));
//    bitFigure->rows.at(1) = dynamic_bitset<unsigned char>(string("100"));
//    bitFigure->print();
//    while (bitFigure->rotate()) bitFigure->print();
//
//    dynamic_bitset<unsigned char> bitset1 = dynamic_bitset<unsigned char>(string("1101"));
//    if (bitset1.all()) {
//        cout << "yes" << endl;
//    }
//    cout << bitset1 << endl;
//    for (int i = 0; i < bitset1.size(); i++) {
//        cout << " i = " << i << " bit = " << bitset1[i] << endl;
//    }
//    cout << subset(bitset1, 0, 3) << endl;
    return 0;
}