//
// Created by root on 12/21/17.
//

#ifndef LAB2_TETRAMINO_BITS_H
#define LAB2_TETRAMINO_BITS_H

#include <bitset>
#include <iostream>
#include <vector>
#include <string>
#include <boost/dynamic_bitset.hpp>

using namespace std;
using namespace boost;

struct BitFigure {
    int turns = 0;
    unsigned int n;
    unsigned int m;
    vector<dynamic_bitset<unsigned char>> rows;

    BitFigure() {
        this->n = 1;
        this->m = 4;
//        //rows = new dynamic_bitset<>[n];
        for (int i = 0; i < n; i++)
            rows[i] = dynamic_bitset<unsigned char>();
    }

//    ~BitFigure() {
//        for (int i = 0; i < n; i++) delete rows[i];
//        delete rows;
//    }

    BitFigure(unsigned int n, unsigned int m) {
        this->n = n;
        this->m = m;
        rows = vector<dynamic_bitset<unsigned char>>(n);
        for (int i = 0; i < n; i++)
            rows[i] = dynamic_bitset<unsigned char>(m);
    }

    explicit BitFigure(BitFigure *bitFigure) {
        this->n = bitFigure->n;
        this->m = bitFigure->m;
        rows = vector<dynamic_bitset<unsigned char>>(n);
        for (int i = 0; i < n; i++) {
            string buffer;
            to_string(bitFigure->rows[i], buffer);
            rows[i] = dynamic_bitset<unsigned char>(buffer);
        }
    }

    void print() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cout << rows[i][j] << " ";
            }
            cout << endl;
        }
    }

    bool rotate() {
        if (turns == 0) return false;

        turns--;
        unsigned int newN = m;
        unsigned int newM = n;
        vector<dynamic_bitset<unsigned char>> newRows = vector<dynamic_bitset<unsigned char>>(newN);
        for (int i = 0; i < newN; i++) {
            newRows[i] = dynamic_bitset<unsigned char>(newM);
            for (int j = 0; j < newM; j++) {
                newRows[i][j] = rows[n - j - 1][i];
            }
        }
        n = newN;
        m = newM;
        rows = newRows;
        return true;
    }

    dynamic_bitset<unsigned char> rowToMultiplier(int row, int y, int fieldSize) {
        dynamic_bitset<unsigned char> result = dynamic_bitset<unsigned char>(fieldSize);
        //cout << "figure row " << rows[row] << endl;
        for (int j = 0; j < m; j++) {
            result[y + j] = rows[row][j];
        }
        return result;
    }
};

class TetraminoBits {
private:
    vector<dynamic_bitset<unsigned char>> field;
    //vector<dynamic_bitset<unsigned char>> *multipliers;
    unsigned int size = 6;
    vector<BitFigure> *figures;

    void initFigures(int size) {
        figures = new vector<BitFigure>();
        BitFigure *bitFigure;

        // x x x x
        bitFigure = new BitFigure(1, 4);
        bitFigure->turns = 1;
        bitFigure->rows.at(0) = dynamic_bitset<unsigned char>(string("1111"));
        figures->push_back(*bitFigure);

        // x x
        // x x
        bitFigure = new BitFigure(2, 2);
        bitFigure->turns = 0;
        bitFigure->rows.at(0) = dynamic_bitset<unsigned char>(string("11"));
        bitFigure->rows.at(1) = dynamic_bitset<unsigned char>(string("11"));
        figures->push_back(*bitFigure);

        // x x
        //   x x
        bitFigure = new BitFigure(2, 3);
        bitFigure->turns = 3;
        bitFigure->rows.at(0) = dynamic_bitset<unsigned char>(string("110"));
        bitFigure->rows.at(1) = dynamic_bitset<unsigned char>(string("011"));
        figures->push_back(*bitFigure);

        //   x x
        // x x
        bitFigure = new BitFigure(2, 3);
        bitFigure->turns = 3;
        bitFigure->rows.at(0) = dynamic_bitset<unsigned char>(string("011"));
        bitFigure->rows.at(1) = dynamic_bitset<unsigned char>(string("110"));
        figures->push_back(*bitFigure);

        // x x x
        // x
        bitFigure = new BitFigure(2, 3);
        bitFigure->turns = 3;
        bitFigure->rows.at(0) = dynamic_bitset<unsigned char>(string("111"));
        bitFigure->rows.at(1) = dynamic_bitset<unsigned char>(string("100"));
        figures->push_back(*bitFigure);

        // x x x
        //     x
        bitFigure = new BitFigure(2, 3);
        bitFigure->turns = 3;
        bitFigure->rows.at(0) = dynamic_bitset<unsigned char>(string("111"));
        bitFigure->rows.at(1) = dynamic_bitset<unsigned char>(string("001"));
        figures->push_back(*bitFigure);

        // x x x
        //   x
        bitFigure = new BitFigure(2, 3);
        bitFigure->turns = 3;
        bitFigure->rows.at(0) = dynamic_bitset<unsigned char>(string("111"));
        bitFigure->rows.at(1) = dynamic_bitset<unsigned char>(string("010"));
        figures->push_back(*bitFigure);
    }

    bool checkFinished() {
        for (int i = 0; i < size; i++) {
            if (!field.at(i).all()) return false;
        }
        return true;
    }

    bool solveRecursive(int *iteration) {
        *iteration = *iteration + 1;

        vector<BitFigure*> *figuresCopy = new vector<BitFigure*>(7);
        for (int i = 0; i < figures->size(); i++) {
            figuresCopy->at(i) = new BitFigure(&figures->at(i));
        }
        int r;
        while (!figuresCopy->empty()) {
            r = rand() % figuresCopy->size();
            BitFigure *figure = figuresCopy->at(r);
            figuresCopy->erase(figuresCopy->begin() + r);

            for (int i = 0; i <= size - figure->n; i++) {
                for (int j = 0; j <= size - figure->m; j++) {
                    do {
                        bool canInsert = true;
                        vector<dynamic_bitset<unsigned char>*> *multipliers = new vector<dynamic_bitset<unsigned char>*>();
                        for (int rowNumber = 0; rowNumber < figure->n; rowNumber++) {
                            //dynamic_bitset<unsigned char> multiplier = figure->rowToMultiplier(rowNumber, j, size);
                            dynamic_bitset<unsigned char> *pMultiplier = new dynamic_bitset<unsigned char>();
                            *pMultiplier = figure->rowToMultiplier(rowNumber, j, size);
                            multipliers->push_back(pMultiplier);
                            cout << "Multiplier " << *pMultiplier << endl;
                            cout << "Field row " << field.at(i + rowNumber) << endl;
                            dynamic_bitset<unsigned char> res = ~(field.at(i + rowNumber) & *pMultiplier);
                            cout << "Res " << res << endl;
                            canInsert = canInsert && res.all();
                        }
                        if (canInsert) {
                            for (int rowNumber = 0; rowNumber < figure->n; rowNumber++) {
                                //dynamic_bitset<unsigned char> area = subset(field.at(rowNumber + j)., i + j + field.size() * rowNumber, figure->m);
                                //couldInsert = couldInsert && (~(area&=figure->rows[i])).all();
                                cout << "field before " << field.at(i + rowNumber)  << endl;
                                cout << "multiplier before " << *multipliers->at(rowNumber)  << endl;
                                field.at(i + rowNumber) |= *multipliers->at(rowNumber);
                                cout << "field after " << field.at(i + rowNumber)  << endl;
                            }
                            cout << "Succeed on iteration " << *iteration << endl;
                            printField();
                            if (checkFinished()) return true;
                            if (solveRecursive(iteration)) {
//                                for (int rowNumber = 0; rowNumber < figure->n; rowNumber++) {
//                                    delete multipliers->at(rowNumber);
//                                }
                                delete multipliers;
                                delete figure;
                                for (int k = 0; k < figuresCopy->size(); k++) {
                                    delete figuresCopy->at(k);
                                }
                                delete figuresCopy;
                                return true;
                            }
                        } else {
//                            for (int rowNumber = 0; rowNumber < figure->n; rowNumber++) {
//                                delete multipliers->at(rowNumber);
//                            }
                            delete multipliers;
                        }
                    } while (figure->rotate());
                }
            }
            delete figure;
        }
        for (int k = 0; k < figuresCopy->size(); k++) {
            delete figuresCopy->at(k);
        }
        delete figuresCopy;
        return false;
    }

public:
    TetraminoBits() {
        size = 6;
        field = vector<dynamic_bitset<unsigned char>>(size);
        for (int i = 0; i < size; i++) field[i] = dynamic_bitset<unsigned char>(size);
        initFigures(size);
    }

    explicit TetraminoBits(unsigned int size) {
        this->size = size;
        field = vector<dynamic_bitset<unsigned char>>(size);
        for (int i = 0; i < size; i++) field[i] = dynamic_bitset<unsigned char>(size);
        initFigures(size);
    }

    void printField() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                cout << field[i][j] << " ";
            }
            cout << endl;
        }
    }

    int solve() {
        srand(time(NULL));
        int *iteration = new int(0);
        //multipliers = new vector<dynamic_bitset<unsigned char>>();
        printField();
        solveRecursive(iteration);
        cout << "Took " << *iteration << " iterations. Result:" << endl;
        printField();
    }
};

#endif //LAB2_TETRAMINO_BITS_H
